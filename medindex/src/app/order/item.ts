export class Item {
  id: number;
  code: string;
  name: string;
  price: number;
  status: string;
}
