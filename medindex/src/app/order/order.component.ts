import {Component, OnInit} from '@angular/core';

import {nomenclatures} from '../nomenclatures-data';
import {nomenclaturePrices} from '../nomenclature-prices';
import {Item} from './item';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent implements OnInit {

  nomenclatures: Item[] = nomenclatures;
  nomenclaturePrices = nomenclaturePrices;
  public availableNomenclatures: any = [];

  constructor() {
  }

  getSum(): number {
    this.availableNomenclatures = nomenclatures.filter(item => item.status === 'Available');
    return this.availableNomenclatures.length ? this.availableNomenclatures
      .map(item => item.price)
      .reduce((prev, next) => prev + next) : 0;
  }

  sort() {
    this.nomenclatures.sort((a, b) => {
      const st = ('' + a.status).localeCompare(b.status);
      return st === 0 ? ('' + a.code).localeCompare(b.code) : st;
    });
  }

  delete(i) {
    this.nomenclatures.splice(i, 1);
  }

  recalculate() {
    this.nomenclatures.map(obj => {
      const existing = this.nomenclaturePrices.find(o => o.id === obj.id);
      return Object.assign(obj, existing);
    });
    this.sort();
  }

  ngOnInit() {
    this.getSum();
    this.sort();
  }

}
