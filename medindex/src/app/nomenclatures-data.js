export const nomenclatures = [
	{
		id: 1,
		code: '02-001',
		name: 'Анализ кала на скрытую кровь',
		price: 330,
		status: 'Available'		
	},
	{
		id: 2,
		code: '90-253',
		name: 'Услуга по подготовке к сбору кала',
		price: 20,
		status: 'Available'		
	},
	{
		id: 3,
		code: '06-108',
		name: 'Витамин к (филлохинон)',
		price: 2510,
		status: 'Available'		
	},
	{
		id: 4,
		code: '90-001',
		name: 'Взятие крови из периферической вены',
		price: 170,
		status: 'Available'		
	},
	{
		id: 5,
		code: '21-017',
		name: 'Аллергин с204 - амоксициллин, IgE',
		price: 600,
		status: 'Unavailable'		
	},
	{
		id: 6,
		code: '02-002',
		name: 'Анализ мочи по Нечипоренко',
		price: 275,
		status: 'Unavailable'		
	},
	{
		id: 7,
		code: '90-563',
		name: 'Услуга по сбору разовой порции мочи с консервантом (хлоргексилин)',
		price: 30,
		status: 'Unavailable'		
	}
];
